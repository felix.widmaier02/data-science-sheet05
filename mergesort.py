import matplotlib.pyplot as plt  # change: added the import statement to the top of the file


def insert_list(data: list, sublist: list, start: int, counter: int):
    """
    Some function for inserting a sublist into a bigger list.
    This function will change all values in data starting from the counter
    index up to the counter index + the length of the given sublist
    and overwrite the values of data[i + counter] <- sublist[i + start]
    where i ranges from 0 to the length of the sublist - 1.
    :param data: some bigger list where you want to change values
    :param sublist: some smaller (sub-)list of the bigger list
    :param start: the starting index for the smaller list
    :param counter: the starting index for the bigger list
    :return:
    """

    # change: got rid of unnecessarily used counter variables when you can just do it with one
    for i in range(len(sublist) - start):
        data[counter + i] = sublist[start + i]


def merge_sort(data: list):  # change: no camel case; shorter variable names
    """
    Sorts the given list 'data' using merge sort.
    After calling this method in the list, the
    list will be sorted. There is no return.


    :param data: the list that shall be sorted.
    """
    length = len(data)
    if length <= 1:  # empty lists and singletons do not need to be sorted
        return

    mid = length // 2
    left = data[:mid]
    right = data[mid:]

    # recursive call to merge_sort on the left and right parts of the
    # list.
    merge_sort(left)
    merge_sort(right)

    left_index = 0  # change: more meaningful variable names
    right_index = 0
    counter_index = 0

    while left_index < len(left) and right_index < len(right):
        if left[left_index] <= right[right_index]:
            # change: got rid of 'assignment' as it just makes the code more
            # convoluted and harder to read
            data[counter_index] = left[left_index]
            left_index += 1
        else:
            data[counter_index] = right[right_index]
            right_index += 1
        counter_index += 1

    # change: got rid of repeating code and made a function for that
    insert_list(data, left, left_index, counter_index)
    counter_index += len(left) - left_index
    insert_list(data, right, right_index, counter_index)


def plot_list(data: list):
    """
    A simple method for plotting a given list (of integers) using matplotlib
    :param data: the list that should be plotted
    :return:
    """
    x = range(len(data))
    plt.plot(x, data)
    plt.show()


if __name__ == "__main__":
    my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    plot_list(my_list)
    merge_sort(my_list)
    plot_list(my_list)
